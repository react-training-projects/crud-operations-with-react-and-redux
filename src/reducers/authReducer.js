import { SIGN_IN, SIGN_OUT } from '../actions/types';

const INIT_STATE = {
  isSignedIn: null,
  userId: null
};

export default (state = INIT_STATE, action) => {
  var result;

  switch (action.type) {
    case SIGN_IN:
      result = { ...state, isSignedIn: true, userId: action.payload };
      break;
    case SIGN_OUT:
      result = { ...state, isSignedIn: false, userId: null };
      break;
    default:
      result = state;
  }

  return result;
};