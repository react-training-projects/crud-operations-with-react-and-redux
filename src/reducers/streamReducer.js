import _ from 'lodash';

import { 
  CREATE_STREAM,
  GET_STREAMS,
  GET_STREAM,
  EDIT_STREAM,
  DELETE_STREAM
} from '../actions/types';

export default (state = {}, action) => {
  switch (action.type) {
    case CREATE_STREAM:
    case GET_STREAM:
    case EDIT_STREAM:
      state = { ...state, [action.payload.id]: action.payload };
      break;
    case GET_STREAMS:
      var mapped = _.mapKeys(action.payload, 'id');
      state = { ...state, ...mapped };
      break;
    case DELETE_STREAM:
      state = _.omit(state, action.payload);
      break;
    default:
  }

  return state;
};
