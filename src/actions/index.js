import streams from '../apis/streams';
import history from '../history';
import { 
  SIGN_IN,
  SIGN_OUT,
  CREATE_STREAM,
  GET_STREAM,
  GET_STREAMS,
  EDIT_STREAM,
  DELETE_STREAM
} from './types';
export const signIn = userId => {
  return {
    type: SIGN_IN,
    payload: userId
  };
};

export const signOut = () => {
  return {
    type: SIGN_OUT
  };
};

export const createStream = vals => async (dispatch, getState) => {
  const { userId } = getState().auth;
  console.log(getState());
  const response = await streams.post('/streams', { ...vals, userId });
  const action = {
    type: CREATE_STREAM,
    payload: response.data
  };

  dispatch(action);
  history.push('/');
};

export const fetchStreams = () => async dispatch => {
  const response = await streams.get('/streams');
  const action = {
    type: GET_STREAMS,
    payload: response.data
  };

  dispatch(action);
};

export const fetchStream = id => async dispatch => {
  const response = await streams.get(`/streams/${id}`);
  const action = {
    type: GET_STREAM,
    payload: response.data
  };

  dispatch(action);
};

export const editStream = (id, vals) => async dispatch => {
  const response = await streams.patch(`/streams/${id}`, vals);
  const action = {
    type: EDIT_STREAM,
    payload: response.data
  };

  dispatch(action);
  history.push('/');
};

export const deleteStream = id => async dispatch => {
  await streams.delete(`/streams/${id}`);
  const action = {
    type: DELETE_STREAM,
    payload: id
  };

  dispatch(action);
  history.push('/');
};
