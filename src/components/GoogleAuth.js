import React from 'react';
import { connect } from 'react-redux';
import { signIn, signOut } from '../actions';

const CLIENT_ID = 
  '580435180995-4joc5c2qkq1n4ppl9r1brstmskf0nvf2.apps.googleusercontent.com';

class GoogleAuth extends React.Component {

  componentDidMount() {
    window.gapi.load('client:auth2', () => {
      window.gapi.client.init({
        clientId: CLIENT_ID,
        scope: 'email'
      }).then(() => {
        this.auth = window.gapi.auth2.getAuthInstance();

        this.onAuthChange(this.auth.isSignedIn.get());
        this.auth.isSignedIn.listen(this.onAuthChange);
      });
    });
  }

  onAuthChange = (isSignedIn) => {
    if (isSignedIn) {
      this.props.signIn(this.auth.currentUser.get().getId());
    } else {
      this.props.signOut();
    }
  };

  onSignInClick = () => {
    this.auth.signIn();
  };

  onSignOutClick = () => {
    this.auth.signOut();
  };

  renderAuthButton() {
    var content;

    if (this.props.isSignedIn === null) {
      content = null;
    } else if (this.props.isSignedIn) {
      content = (
        <button 
            onClick={this.onSignOutClick}
            className="ui red google button">
          <i className="google icon" />
          Sign Out
        </button>
      );
    } else {
      content = (
        <button 
            onClick={this.onSignInClick}
            className="ui red google button">
          <i className="google icon" />
          Sign In
        </button>
      );
    }

    return(
      <div className="auth-link">{content}</div>
    );
  }

  render() {
    return (
      <div className="googl-auth item">
        {this.renderAuthButton()}
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    isSignedIn: state.auth.isSignedIn,
    userId: state.auth.userId
  };
};

export default connect(mapStateToProps, { signIn, signOut })(GoogleAuth);
