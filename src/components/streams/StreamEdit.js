import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import { fetchStream, editStream } from '../../actions';
import StreamForm from './StreamForm';

class StreamEdit extends React.Component {
  componentDidMount() {
    this.props.fetchStream(this.props.match.params.id);
  }

  onSubmit = formVals => {
    this.props.editStream(this.props.stream.id, formVals);
  }

  renderContent() {
    var content = 'Form loading...'
    
    if (this.props.stream) {
      content = <StreamForm 
        initialValues={
          _.pick(this.props.stream, 'title', 'description')
        }
        onSubmit={this.onSubmit} />;
    }

    return content;
  }

  render() {
    console.log(this.props)
    return (
      <div className="stream-edit">
        <h3>Edit a Stream</h3>
        {this.renderContent()}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    stream: state.streams[ownProps.match.params.id],
    editStream
  };
};

export default connect(mapStateToProps, {
  fetchStream,
  editStream
})(StreamEdit);
