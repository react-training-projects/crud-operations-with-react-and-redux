import React from 'react';
import { connect } from 'react-redux';
import flv from 'flv.js';

import { fetchStream } from '../../actions';

class StreamShow extends React.Component {
  constructor(props) {
    super(props);

    this.videoRef = React.createRef();
  }

  componentDidMount() {
    this.props.fetchStream(this.props.match.params.id);
    this.buildPlayer();
  }

  componentDidUpdate() {
    this.buildPlayer();
  }

  buildPlayer() {
    var player = null;
    var playerExists = !this.player || this.player.exists;

    if (!playerExists && this.props.stream) {
      const id = this.props.match.params.id;
      this.player = flv.createPlayer({
        type: 'flv',
        url: `http://localhost:8000/live/${id}.flv`
      });

      this.player.attachMediaElement(this.videoRef.current);
      this.player.load();
    }

    return player;
  }

  componentWillUnmount() {
    if (this.player && this.player.exists) {
      this.player.destroy();
    }
  }

  renderContent() {
    var content = 'Loading . . .';

    if (this.props.stream) {
      const { title, description } = this.props.stream;
      content = (
        <>
          <video
              controls
              style={{ width: '100%' }}
              ref={this.videoRef} />
          <h1>{title}</h1>
          <h5>{description}</h5>
        </>
      );  
    }

    return content;
  }

  render() {
    return (
      <div className="stream-show">
        {this.renderContent()}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    stream: state.streams[ownProps.match.params.id]
  };
}

export default connect(mapStateToProps, { fetchStream })(StreamShow);
