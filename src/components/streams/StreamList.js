import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { fetchStreams } from '../../actions';

class StreamList extends React.Component{
  state = {};

  componentDidMount() {
    this.props.fetchStreams();
  }

  renderCreate() {
    var content = null;

    if (this.props.isSignedIn) {
      content = (
        <div style={{ textAlign: 'right' }}>
          <Link to="/streams/new" className="ui button primary">
            Create
          </Link>
        </div>
      );
    }

    return content;
  }

  renderAdmin(stream) {
    var admin = null;

    if (stream.userId === this.props.currentUserId) {
      admin = (
        <div className="right floated content">
          <Link 
              to={`/streams/edit/${stream.id}`}
              className="ui button primary">
            Edit
          </Link>
          <Link 
              to={`/streams/delete/${stream.id}`}
              className="ui button negative">
            Delete
          </Link>
        </div>
      );
    }

    return admin;
  }

  renderList() {
    return this.props.streams.map(stream => {
      return (
        <div className="item" key={stream.id}>        
          {this.renderAdmin(stream)}
          <i className="large middle aligned icon camera" />
          <div className="content">
            <Link to={`/streams/${stream.id}`} className="header">
              {stream.title}
            </Link>
            <div className="description">
              {stream.description}
            </div>
          </div>
        </div>
      );
    })
  }

  render() {
    return (
      <div className="stream-create">
        <h2>Streams</h2>
        <div className="ui celled list">
          {this.renderList()}
        </div>
        {this.renderCreate()}
      </div>
    );
  };
}

const mapStateToProps = state => {
  return {
    streams: Object.values(state.streams),
    currentUserId: state.auth.userId,
    isSignedIn: state.auth.isSignedIn
  };
};

export default connect(mapStateToProps, { fetchStreams })(StreamList);
