import React from 'react';
import { Field, reduxForm } from 'redux-form';

class StreamForm extends React.Component {
  renderError = ({ touched, error }) => {
    if (touched && error) {
      return (
        <div className="ui error message">
          <div className="header">{error}</div>
        </div>
      );
    }
  }

  renderInput = ({ input, label, meta }) => {
    const error = meta.error && meta.touched;
    const className = `field ${error ? 'error' : ''}`;

    return (
      <div className={className}>
        <label>{label}</label>
        <input {...input} />
        {this.renderError(meta)}
      </div>
    );
  };

  onSubmit = (formVals) => {
    this.props.onSubmit(formVals);
  };

  render() {
    return (
      <div className="stream-create">
        <form
            onSubmit={this.props.handleSubmit(this.onSubmit)}
            className="ui form error">
          <Field
              name="title"
              component={this.renderInput}
              label="Title" />
          <Field
              name="description"
              component={this.renderInput}
              label="Description" />
          <button className="ui button primary">Submit</button>
        </form>
      </div>
    );
  }
}

const validate = (formVals) => {
  const errors = {};

  if (!formVals.title) {
    errors.title = 'You must enter a title';
  }

  if (!formVals.description) {
    errors.description = 'You must enter a description';
  }

  return errors;
};

export default reduxForm({
  form: 'streamForm',
  validate: validate
})(StreamForm);
